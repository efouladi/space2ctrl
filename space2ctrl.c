
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <linux/input.h>

// clang-format off
const struct input_event
space_up        = {.type = EV_KEY, .code = KEY_SPACE,    .value = 0},
ctrl_up         = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 0},
space_down      = {.type = EV_KEY, .code = KEY_SPACE,    .value = 1},
ctrl_down       = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 1},
space_repeat    = {.type = EV_KEY, .code = KEY_SPACE,    .value = 2},
ctrl_repeat     = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 2},
syn             = {.type = EV_SYN, .code = SYN_REPORT,   .value = 0},
shift_right_down      = {.type = EV_KEY, .code = KEY_RIGHTSHIFT,    .value = 1},
shift_right_repeat    = {.type = EV_KEY, .code = KEY_RIGHTSHIFT,    .value = 2},
shift_left_down       = {.type = EV_KEY, .code = KEY_LEFTSHIFT,    .value = 1},
shift_left_repeat     = {.type = EV_KEY, .code = KEY_LEFTSHIFT,    .value = 2},
meta_right_down      = {.type = EV_KEY, .code = KEY_RIGHTMETA,    .value = 1},
meta_right_repeat    = {.type = EV_KEY, .code = KEY_RIGHTMETA,    .value = 2},
meta_left_down       = {.type = EV_KEY, .code = KEY_LEFTMETA,    .value = 1},
meta_left_repeat     = {.type = EV_KEY, .code = KEY_LEFTMETA,    .value = 2};
// clang-format on

int equal(const struct input_event *first, const struct input_event *second) {
    return first->type == second->type && first->code == second->code &&
           first->value == second->value;
}

int read_event(struct input_event *event) {
    return fread(event, sizeof(struct input_event), 1, stdin) == 1;
}

void write_event(const struct input_event *event) {
    if (fwrite(event, sizeof(struct input_event), 1, stdout) != 1)
        exit(EXIT_FAILURE);
}

int main(void) {
    int space_is_ctrl = 0;
    struct input_event input, key_down, key_up, key_repeat;
    enum { START, SPACE_HELD, KEY_HELD } state = START;

    setbuf(stdin, NULL), setbuf(stdout, NULL);

    while (read_event(&input)) {
        if (input.type == EV_MSC && input.code == MSC_SCAN)
            continue;

        if (input.type != EV_KEY) {
            write_event(&input);
            continue;
        }

        switch (state) {
            case START:
                if (space_is_ctrl) {
                    if (input.code == KEY_SPACE) {
                        input.code = KEY_LEFTCTRL;
                        if (input.value == 0)
                            space_is_ctrl = 0;
                    }
                    write_event(&input);
                } else {
                    if (equal(&input, &space_down) ||
                        equal(&input, &space_repeat)) {
                        state = SPACE_HELD;
                    } else {
                        write_event(&input);
                    }
                }
                break;
            case SPACE_HELD:
                if (equal(&input, &space_down) || equal(&input, &space_repeat) ||
                    equal(&input, &shift_left_down) || equal(&input, &shift_left_repeat) ||
                    equal(&input, &shift_right_down) || equal(&input, &shift_right_repeat) ||
                    equal(&input, &meta_right_down) || equal(&input, &meta_right_repeat) ||
                    equal(&input, &meta_left_down) || equal(&input, &meta_left_repeat))
                    break;
                if (input.value != 1) {
                    write_event(&space_down);
                    write_event(&syn);
                    usleep(20000);
                    write_event(&input);
                    state = START;
                } else {
                    key_down = key_up = key_repeat = input;
                    key_up.value     = 0;
                    key_repeat.value = 2;
                    state            = KEY_HELD;
                }
                break;
            case KEY_HELD:
                if (equal(&input, &space_down) || equal(&input, &space_repeat))
                    break;
                if (equal(&input, &key_down) || equal(&input, &key_repeat))
                    break;
                if (equal(&input, &key_up)) {
                    write_event(&ctrl_down);
                    space_is_ctrl = 1;
                } else {
                    write_event(&space_down);
                }
                write_event(&syn);
                usleep(20000);
                write_event(&key_down);
                write_event(&syn);
                usleep(20000);
                write_event(&input);
                state = START;
                break;
        }
    }
}
