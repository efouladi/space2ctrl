cmake_minimum_required(VERSION 3.0)

project(space2ctrl)

add_executable(space2ctrl space2ctrl.c)
target_compile_options(space2ctrl PRIVATE -Wall -Wextra)

install(TARGETS space2ctrl RUNTIME DESTINATION bin)
