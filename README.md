# Notice

This code is based on: https://gitlab.com/interception/linux/plugins/space2meta and all
credits goes to its author

# space2ctrl

Turn your space key into the control key when chorded
to another key (on key release only).

## Dependencies

- [Interception Tools][interception-tools]

## Building

```
$ git clone https://gitlab.com/efouladi/space2ctrl.git
$ cd space2ctrl
$ cmake -Bbuild
$ cmake --build build
```

## Execution

`space2ctrl` is an [_Interception Tools_][interception-tools] plugin. A suggested
`udevmon` job configuration is:


```yaml
- JOB: "intercept -g $DEVNODE | space2ctrl | uinput -d $DEVNODE"
  DEVICE:
    EVENTS:
      EV_KEY: [KEY_SPACE]

```

To compose functionality with [`caps2esc`], for example, you do:

```yaml
- JOB: "intercept -g $DEVNODE | caps2esc | space2ctrl | uinput -d $DEVNODE"
  DEVICE:
    EVENTS:
      EV_KEY: [KEY_CAPSLOCK, KEY_ESC, KEY_SPACE]

```

For more information about the [_Interception Tools_][interception-tools], check
the project's website.

## Caveats

As always, there's always a caveat:

- visual delay when inserting space.
- `intercept -g` will "grab" the detected devices for exclusive access.
- If you tweak your key repeat settings, check whether they get reset.
  Please check [this report][key-repeat-fix] about the resolution.

## License

<a href="https://gitlab.com/efouladi/space2ctrl/blob/5f3d6a3da3ddd6164835e2f9b33664e547a85a74/LICENSE.md">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/License_icon-mit-2.svg/120px-License_icon-mit-2.svg.png" alt="MIT">
</a>

Copyright © 2019 Francisco Lopes da Silva

[interception]: https://github.com/oblitum/Interception
[`caps2esc`]: https://gitlab.com/interception/linux/plugins/caps2esc
[interception-tools]: https://gitlab.com/interception/linux/tools
[key-repeat-fix]: https://github.com/oblitum/caps2esc/issues/1
